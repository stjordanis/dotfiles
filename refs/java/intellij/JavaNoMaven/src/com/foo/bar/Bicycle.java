package com.foo.bar;

public class Bicycle extends WheeledVehicle {
    boolean kickstandOut;

    public Bicycle(){
        super(2,1);
        kickstandOut = true;
    }

    public String toString(){
        return "Bicycle: " + super.toString();
    }

    public void switchKickstand(){
        kickstandOut = !kickstandOut;
        System.out.println(kickstandOut ? "kickstand is out" : "kickstand is retracted");
    }
}

package com.foo.bar;

import java.util.LinkedList;

public class AvlTree<V> {

    private Node root;

    private class Node{
        Integer key;
        V val;
        Integer height; // Leaf has height 0.
        Node left;
        Node right;

        public Node(Integer key, V val){
            this.key = key;
            this.val = val;
            this.height = 0;
        }

        public void swapWith(Node b){
            Integer tmpKey = this.key;
            V tmpVal = this.val;

            this.key = b.key;
            this.val = b.val;

            b.key = tmpKey;
            b.val = tmpVal;
        }

        public void rotateLeft(){
            Node x = this;
            Node y = right;
            Node z = right.right;

            y.right = y.left;
            y.left = x.left;
            x.right = z;
            x.left = y;
            x.swapWith(y);

            y.updateHeight(); // Now x.
            x.updateHeight(); // Now y.
        }

        public void rotateRight(){
            Node x = left.left;
            Node y = left;
            Node z = this;

            y.left = y.right;
            y.right = z.right;
            z.left = x;
            z.right = y;
            z.swapWith(y);

            y.updateHeight(); // Now z.
            z.updateHeight(); // Now y.
        }

        public void rotateLeftRight(){
            Node x = left;
            Node y = left.right;

            x.right = y.left;
            y.left = x;
            this.left = y;

            x.updateHeight();
            y.updateHeight();
            updateHeight();

            rotateRight();
        }

        public void rotateRightLeft(){
            Node z = right;
            Node y = right.left;

            z.left = y.right;
            y.right = z;
            this.right = y;

            z.updateHeight();
            y.updateHeight();
            updateHeight();

            rotateLeft();
        }

        public void updateHeight(){
            if(left == null && right == null){
                height = 0;
            }
            else if(left == null && right != null){
                height = right.height + 1;
            }
            else if(left != null && right == null){
                height = left.height + 1;
            }
            else{
                height = Math.max(left.height, right.height) + 1;
            }
        }

        public int getDiff(){
            // If left == null && right.height == 0, returns 1.
            if(left == null && right == null){
                return 0;
            }
            else if(left == null && right != null){
                return (right.height + 1) - 0;
            }
            else if(left != null && right == null){
                return 0 - (left.height + 1);
            }
            else return right.height - left.height;
        }

        public void rebalance(){
            int diff = getDiff();

            if(diff == -2){
                if(left.getDiff() == -1){
                    rotateRight();
                }
                else{
                    rotateLeftRight();
                }
            }
            else if(diff == 2){
                if(right.getDiff() == 1){
                    rotateLeft();
                }
                else{
                    rotateRightLeft();
                }
            }
        }

        public void insert(Integer k, V v){
            if(k == key){
                val = v;
                // No height update needed.
                // No rebalance needed.
            }
            else if(k < key){
                if(left == null){
                    left = new Node(k, v);
                    updateHeight();
                    // No rebalance needed.
                }
                else{
                    left.insert(k, v);
                    updateHeight();
                    rebalance();
                }
            }
            else{ // k > key
                if(right == null){
                    right = new Node(k, v);
                    updateHeight();
                    // No rebalance needed.
                }
                else{
                    right.insert(k, v);
                    updateHeight();
                    rebalance();
                }
            }
        }

        public V get(Integer k){
            if(k == key){
                return val;
            }
            else if(k < key){
                if(left == null){ return null; }
                else{ return left.get(k); }
            }
            else{
                if(right == null){ return null; }
                else{ return right.get(k); }
            }
        }

        public void getKeys(LinkedList<Integer> ret){
            if(left != null){ left.getKeys(ret); }
            ret.add(key);
            if(right != null){ right.getKeys(ret); }
        }

        public V remove(Integer k){
            if(k == key){
                if(left == null && right == null){
                    key = null;
                    return val;
                }
                else if(left == null && right != null){
                    V ret = val;
                    key = right.key;
                    val = right.val;
                    left = right.left;
                    right = right.right;
                    updateHeight();
                    return ret;
                }
                else if(left != null & right == null){
                    V ret = val;
                    key = left.key;
                    val = left.val;
                    right = left.right;
                    left = left.left;
                    updateHeight();
                    return ret;
                }
                else{
                    V ret = val;

                    // Promote from left.
                    // Assumes findRightmost() changes height of left by 0 or 1. (?)
                    Node subst = left.findRightmost();
                    if(left.key == null){ left = null; }
                    key = subst.key;
                    val = subst.val;
                    updateHeight();
                    rebalance();

                    return ret;
                }
            }
            else if(k < key){
                if(left == null){
                    return null;
                }
                else{
                    V ret = left.remove(k);
                    if(left.key == null){ left = null; }
                    updateHeight();
                    rebalance();
                    return ret;
                }
            }
            else{
                if(right == null){
                    return null;
                }
                else{
                    V ret = right.remove(k);
                    if(right.key == null){ right = null; }
                    updateHeight();
                    rebalance();
                    return ret;
                }
            }
        }

        public Node findRightmost(){
            if(left == null && right == null){
                Node ret = new Node(key, val);
                key = null;
                return ret;
            }
            else if(right == null){
                Node ret = new Node(key, val);
                key = left.key;
                val = left.val;
                right = left.right;
                left = left.left;
                updateHeight();
                rebalance();
                return ret;
            }
            else{
                Node ret = right.findRightmost();
                if(right.key == null){ right = null; }
                updateHeight();
                rebalance();
                return ret;
            }
        }

        public Integer traverseHeight(){
            if(left == null && right == null){
                return 0;
            }
            else if(left == null && right != null){
                return right.traverseHeight() + 1;
            }
            else if(left != null && right == null){
                return left.traverseHeight() + 1;
            }
            else{
                return Math.max(left.traverseHeight(), right.traverseHeight()) + 1;
            }
        }
    }

    public AvlTree(){
        //
    }

    // If key exists, overwrites existing val.
    public void insert(Integer key, V val){
        if(root == null){
            root = new Node(key, val);
        }
        else{
            root.insert(key, val);
        }
    }

    // If key is found, returns val. Otherwise returns null.
    public V get(Integer key){
        if(root == null){
            return null;
        }
        else{
            return root.get(key);
        }
    }

    // If key is found, returns val. Otherwise returns null.
    public V remove(Integer key){
        if(root == null){
            return null;
        }
        else{
            V ret = root.remove(key);
            if(root.key == null){ root = null; }
            return ret;
        }
    }

    public LinkedList<Integer> getKeys(){
        LinkedList<Integer> ret = new LinkedList<>();
        root.getKeys(ret);
        return ret;
    }

    public Integer getHeight(){
        int traverse = root.traverseHeight();
        if(traverse != root.height){ throw new RuntimeException("Traverse height did not match root height."); }
        return root.height;
    }
}

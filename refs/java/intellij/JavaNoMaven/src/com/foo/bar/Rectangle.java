package com.foo.bar;

public class Rectangle {
    int w;
    int h;

    public Rectangle(int w, int h){
        this.w = w;
        this.h = h;
    }

    public String toString(){
        return "(" + w + "," + h + ")";
    }
}

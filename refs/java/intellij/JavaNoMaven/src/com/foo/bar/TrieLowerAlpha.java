package com.foo.bar;

import java.util.ArrayList;
import java.util.LinkedList;

public class TrieLowerAlpha<T> {
    Node root;

    public TrieLowerAlpha(){
        root = new Node();
    }

    private class Node{
        ArrayList<Node> children;
        LinkedList<T> vals;

        public Node(){
            children = new ArrayList<Node>();
            for(int i = 0; i < 26; i++){
                children.add(null);
            }
            vals = new LinkedList<T>();
        }
    }

    public void insert(String s, T t){
        Node curr = root;
        char[] chars = s.toCharArray();
        int[] ints = new int[chars.length];
        for(int i = 0; i < chars.length; i++){ ints[i] = (int)chars[i] - 97; } // (int)'a'
        int intsIdx = 0;

        while(intsIdx < ints.length){
            if(curr.children.get(ints[intsIdx]) == null){
                curr.children.set(ints[intsIdx], new Node());
            }
            curr = curr.children.get(ints[intsIdx]);
            intsIdx++;
        }

        // Note: If s is an empty string, curr will be pointed to root here.
        // This is not a problem. We can allow the trie to store values for empty strings.

        curr.vals.add(t);
    }

    public LinkedList<T> get(String s){
        Node curr = root;
        char[] chars = s.toCharArray();
        int[] ints = new int[chars.length];
        for(int i = 0; i < chars.length; i++){ ints[i] = (int)chars[i] - 97; } // (int)'a'
        int intsIdx = 0;

        while(intsIdx < ints.length){
            if(curr.children.get(ints[intsIdx]) == null){
                return new LinkedList<T>();
            }
            curr = curr.children.get(ints[intsIdx]);
            intsIdx++;
        }

        return getRecurse(curr); // Another possible implementation: curr.getRecurse(). Need to think about this...
    }

    private LinkedList<T> getRecurse(Node n){
        LinkedList<T> ret = new LinkedList<T>();
        ret.addAll(n.vals);
        for(int i = 0; i < n.children.size(); i++){
            if(n.children.get(i) != null){
                ret.addAll(getRecurse(n.children.get(i)));
            }
        }

        return ret;
    }
}

package com.foo.bar;

public class GarageDoor extends Door {
    boolean doorRaised;

    public GarageDoor(){
        doorRaised = true;
    }

    public void toggleRaised(){
        if(doorRaised){
            System.out.println("lowering garage door");
        }
        else{
            System.out.println("raising garage door");
        }
        doorRaised = !doorRaised;
    }

    public void makeSecure(){
        System.out.println("making garage door secure");
        if(doorRaised){ toggleRaised(); }
        else{ System.out.println("garage door is already secure"); }
    }
}

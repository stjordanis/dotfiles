package com.foo.bar;

import java.util.Arrays;

public class MathExp {

    private enum BinaryOp{
        add("+"),
        sub("-"),
        mul("*"),
        div("/");

        private final String s;

        BinaryOp(String s){
            this.s = s;
        }

        public String toString(){
            return s;
        }
    }

    private enum UnaryOp{
        neg("-");

        private final String s;

        UnaryOp(String s){
            this.s = s;
        }

        public String toString(){
            return s;
        }
    }

    private abstract class Exp{
        public abstract int eval();
    }

    private class Lit extends Exp{
        int val;

        public Lit(int val){
            this.val = val;
        }

        public int eval(){
            return val;
        }
    }

    private class Binary extends Exp{
        Exp left;
        BinaryOp op;
        Exp right;

        public Binary(Exp left, BinaryOp op, Exp right){
            this.left = left;
            this.op = op;
            this.right = right;
        }

        public int eval(){
            int ret;
            switch (op){
                case add: ret = left.eval() + right.eval(); break;
                case sub: ret =  left.eval() - right.eval(); break;
                case mul: ret =  left.eval() * right.eval(); break;
                case div: ret =  left.eval() / right.eval(); break;
                default: ret =  -1;
            }
            return ret;
        }
    }

    private class Unary extends Exp{
        UnaryOp op;
        Exp exp;

        public Unary(UnaryOp op, Exp exp){
            this.op = op;
            this.exp = exp;
        }

        public int eval(){
            int ret;
            switch (op){
                case neg: ret = -1 * exp.eval(); break;
                default: ret =  -1;
            }
            return ret;
        }
    }

    private class Root extends Exp{
        Exp exp;

        public Root(){
            //
        }

        public int eval(){
            return exp.eval();
        }
    }

    private Exp root;

    public int eval(){
        return root.eval();
    }

    private int getLengthParen(String[] tokens, int i){
        int j = i;
        while(!tokens[j].equals(")")){
            j++;
        }
        return j - i + 1; // todo: stopped here
    }

    private Exp parse(String[] tokens){
        root = new Binary(new Lit(0), BinaryOp.add, null);

        for(int i = 0; i < tokens.length; i++){
            String currToken = tokens[i];
            if(currToken.equals("(")){
                int length = getLengthParen(tokens, i);
            }
            else if(currToken.equals(UnaryOp.neg.toString())){
            }
            else{
                int a = Integer.parseInt(currToken);
                Binary b = (Binary)root;
                b.right = new Lit(a);
                if(i < tokens.length - 1){
                    // we have not reached end of string, so next token must be an op, followed by another exp.
                    i++;
                    BinaryOp newOp = null;
                    switch(tokens[i]){
                        case "+": newOp = BinaryOp.add; break;
                        case "-": newOp = BinaryOp.sub; break;
                        case "*": newOp = BinaryOp.mul; break;
                        case "/": newOp = BinaryOp.div; break;
                    }
                    root = new Binary(root, newOp, null);
                }

            }
        }
        return root;
    }

    public MathExp(String s){
        String[] tokens = s.split(" ");
        root = parse(tokens);
    }
}

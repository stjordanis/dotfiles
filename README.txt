$ cd ~
$ git clone https://BitPusher16@bitbucket.org/BitPusher16/dotfiles.git
$ cd dotfiles
$ chmod +x insert_includes.sh
$ ./insert_includes.sh

I have tried several schemes for quickly installing my dotfiles on a new system.
My first attempt was to use a script to backup the system's default dotfiles
and replace them with dynamic links to the dotfiles in my repo.
However, this caused me to lose all settings in the default system dotfiles.
My next attempt was to use a script to append the contents of my dotfiles
to the system dotfiles.
This was a poor approach because any changes I made to the dotfiles after
appending could not be pushed back to my master dotfile repo.
My current approach is to use a script to insert a single include statement
into each system dotfile.
This include statement grabs the contents of my repo dotfiles.
This has been working well.

Note to self: After cloning this repo to a new machine, consider creating
an SSH key pair on the machine and adding the public key to Bitbucket.
One gotcha: Bitbucket doesn't support per-repo keys. The public SSH key
must be registered under your user account.

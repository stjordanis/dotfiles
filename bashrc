# clean up bash history;
HISTCONTROL=ignoredups # ignorespace, ignoredups, ignoreboth, erasedups;
# increase num lines kept in memory;
HISTSIZE=10000
# increase num lines stored on disk;
HISTFILESIZE=10000
shopt -s histappend

# update window size after each command;
# forgot what this is for... vim? screen?
#shopt -s checkwinsize

# prevent Ctrl-s from calling flow control method XOFF, freezing the terminal;
# (also frees Ctrl-q (XON));
stty ixoff -ixon

# provide the cl (change directory + pwd + list) command;
function cl() {
    if [ "$#" -eq 1 ]; then
        cd "$1";
        pwd;
        ls;
    else
        cd ~;
        pwd;
        ls;
    fi
}
export -f cl

# enable vim mode
set -o vi

# prevent clobber via >;
set -o noclobber

# start tmux; this must come last so that settings apply to all tmux windows?
# the exec command replaces the bash process;
# edit: if you close the terminal accidentally, this setting makes it difficult to kill the tmux sessions?

#if command -v tmux>/dev/null; then
#	[[ ! $TERM =~ screen ]] && [ -z $TMUX ] && exec tmux
#fi

if command -v tmux>/dev/null; then
	[[ ! $TERM =~ screen ]] && [ -z $TMUX ] && tmux
fi

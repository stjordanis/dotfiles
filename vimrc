" reload .vimrc with ':source $MYVIMRC' or ':source %' if the vimrc buffer is in focus;
" expand all tabs in this file with zR;

" todo: prevent vim :w clobber;
" todo: one of these settings is making vim use spaces in .py files...

"----------
" internals;
"----------

set nocompatible          " turn off Vi compatibility; set this first;
filetype plugin on        " help page for netrw says enable this;
set undodir=~/dotfiles/swp/
                          " set undo, backup, swap dirs;
                          " these directories must be created manually; 
                          " append '//' to ensure unique names for the backup files?
                          " but '//' only works for swap and undo files? 
                          " https://stackoverflow.com/questions/26742313/
                          " why-vim-backup-filenames-are-not-correct-backupdir-option-not-performing-as-e
set backupdir=~/dotfiles/swp/
set directory=~/dotfiles/swp/
set key=                  " sets key to blank string so files accidentally encrypted with :X can be recovered;
set encoding=utf-8
set hidden                " allow switching away from unsaved buffers;
"set autowrite             " automatically write file on switching to another file;

"----------
" tab behavior;
"----------

set autoindent            " auto-indent;
set tabstop=4             " how wide to draw tab characters; warning! messes with foldmethod=indent somehow; see shiftwidth below;
set shiftwidth=4          " num spaces to indent or dedent when using '<' and '>'
                          " NOTE! must set this along with tabstop for foldmethod=indent to work;
"set expandtab             " insert spaces rather than tabs when tab key is pushed;
"set softtabstop=2         " target number of columns when tab or backspace is pressed;
"set shiftround            " always indent/outdent to the nearest tabstop;
"set noshiftround          " undo set shiftround;
"set smarttab              " use tabs at the start of a line, spaces elsewhere;
"filetype indent on        " activates indenting for files;

"----------
" visual;
"----------

set shortmess+=I          " disable splash screen
"let g:netrw_banner=0      " disable netrw banner
"set vb t_vb=              " enable visual bell to disable audio bell, then disable visual bell to remove both
set wildmenu              " enable colon-command tab completion;
set wildignorecase
set wildmode=longest:list,full 
                          " 1st <Tab> completes longest string, 
                          " 2nd <Tab> opens wild menu list, completes to first full match; 
                          " (try with :color d<Tab>)
set number                " show line numbers
set ruler                 " show line number, char number, file poisition in status line
set laststatus=2          " turn on status line
set colorcolumn=81        " show a column guide; the 81st column is colored; alt: cc=81;
hi ColorColumn ctermbg=yellow 
                          " set column guide color to yellow;
set showtabline=2         " always show the tab line, even if only one tab is open
set nohlsearch            " don't continue to highlight searched phrases.
set incsearch             " but do highlight as you type your search.
set ignorecase            " make searches case-insensitive.
"set list                  " show invisible characters;
"nmap <leader>l :set list!<CR>
                          " set \l to toggle invisible characters
"set listchars=eol:¬,tab:>-,trail:· 
                          " use better symbols for some invisible chars
                          "to view unicode symbols in Putty, set Remote character set to UFT-8
                          "to enter unicode chars: ctrl-k N O gives ¬, ctrl-k ~ . gives ·
"set listchars=eol:¬,tab:>-,trail:·,extends:>,precedes:<
"set listchars=eol:¬,tab:¦­,trail:~,extends:>,precedes:< "
                          " unicode (Ctrl+v) 172, 166, and 173 (blank)
                          " doesn't play well with folding?
                          " add chars for leading spaces and \r?
"set listchars=eol:¬,tab:¦\ ,trail:~,extends:>,precedes:< 
                          " unicode (Ctrl+v) 172 and 166
set wrap lbr              " for long lines, wrap between word breaks
"set diffopt+=vertical     " :diffsplit opens a vertical split
au FileType * setlocal comments-=:// comments-=:#  comments-=:\" 
                          " disable next line auto-comment; block comments still enabled;
hi MatchParen ctermfg=black ctermbg=grey
set scrolloff=1000



"----------
" folding;
"----------

" would it be better to just have better bracket navigation?
" something as simple as jumping to next } or previous { would work...

set foldmethod=indent      " tell vim how to find folds;
set foldlevel=20           " when file opens, start with a high level of unfolds
"set foldclose=all         " close fold when navigating out of it
" (zo:open fold) (zc: close fold) (zm:increase auto fold depth) (zr:reduce auto fold depth)
nnoremap <Space> za

"----------
" key mappings;
"----------

" change leader;
let mapleader=","

" remap semicolon
"nnoremap ; :

" automatically center vertically during search
"nnoremap n nzz
"nnoremap N Nzz

" enable jumping to beginning of last section even without bracket on its own line; :help [[ for details;
"nnoremap [[ ?{<CR>w99[{
"nnoremap ]] j0?{<CR>w99[{%/{<CR> " note: example in vim help relies on recursion;
" enable folding of outermost section
"nmap z[ [[jzf]]

" faster navigation
"nnoremap ;j L
"nnoremap ;k H

"nnoremap H 16h
"nnoremap J 4gjzz
"nnoremap K 4gkzz
"nnoremap L 16l

"nnoremap J 4gj4<C-e>
"nnoremap K 4gk4<C-y>
"nnoremap J 6gj           " fast down
"nnoremap K 6gk           " fast up
"nnoremap H J             " we still need J; map H to it;

" allow better navigation of wrapped lines; nnoremap means normal mode no remap;
nnoremap k gk
nnoremap j gj
nnoremap 0 g0
nnoremap $ g$

" use zz to keep cursor centered instead of scrolloff.
" scrolloff doesn't handle end of file well.
"nnoremap k gkzz
"nnoremap j gjzz
"imap <enter> <enter><C-O>zz
"nmap o o<C-O>zz
"nmap O O<C-O>zz
"nnoremap G Gzz
"vnoremap j jzz
"vnoremap k kzz


" switch ZZ to write all and quit rather than write current and quit;
" disabled for now; I prefer to review which files I've changed;
"nnoremap ZZ :xa<Enter>

" set remap so that braces indent automatically;
" needs autoindent turned on;
" inoremap means this mapping is for insert mode;
inoremap {<Return> {<Return>}<Esc>kA<Return><Tab>

" set remap so that colon-return indents automatically;
" needs autoindent turned on;
"inoremap :<Return> :<Return><Tab>

"set mappings for opening and closing tabs
"map ,e :tabe.<Return>
"map ,c :tabc<Return>
"map ,c :try\|tabc\|catch\|q\|endtry<Return> " attempt to close tab; if last tab, quit; WARNING: does not stop to save;
"map ,n :tabnew<Return>
"map ,q :qa<Return>
"map ,o :tabo<Return> " close all other tabs

"nnoremap ge :tabe.<Return>
"nnoremap gc :tabc<Return>

" easier tab navigation
"nnoremap gn gt
"nnoremap gp gT

" quick buffer selection
"nnoremap <leader>b :ls<Return>:b 
"nnoremap <leader>n :bn<Return>
"nnoremap <leader>p :bp<Return>
"nnoremap <leader>d :bd<Return>

" can also use Tab for buffer navigation;
nnoremap <Tab> :bn<Return>:ls!<Return>
nnoremap <S-Tab> :bp<Return>:ls!<Return>

" Another alternative: use tab completion to list open buffers.
" Also note that tab completion can be narrowed down by entering part of a path.
" For example: :b ~<tab> autocompletes open buffers which are from the home dir.
" Note: This option will start at the beginning of the tab list every time.
" Also, it does not give a "preview" of the files as you cycle through.
"set wildcharm=<tab> " wildchar for macros; 0 by default;
"nnoremap <leader>b :b<space><tab>
	" requies wildcharm=<tab>
set wildcharm=<C-z> " wildchar for macros; 0 by default;
nnoremap <leader>b :b<space><C-z>
	" requires wildcharm=<C-z>

" faster window navigation (in vim, tabs contain windows, windows cycle through open buffers);
" update: these aren't really needed; C-w v gives a convenient vertical split and C-direction is not bad;
"nnoremap <leader>v :vsplit<return>
"nnoremap <leader>s :split<return>
"nnoremap <leader>l <C-w>l
"nnoremap <leader>h <C-w>h
"nnoremap <leader>j <C-w>j
"nnoremap <leader>k <C-w>k

" buffer management;
"nnoremap <leader>d :bd<return>
	" this implementation will close all windows containing the buffer (except last window?)
	" don't really like this, but it's fine since usually I only open a new window to look at one file;
nnoremap <leader>d :bp<bar>bd #<return>
"nnoremap <leader>d :bp\|bd #<return>
"nnoremap <leader>d :enew\|bd #<return>
"nnoremap <leader>q :q<return>
	" note: :q closes only the current window as well?
"nnoremap <leader>q :qa<return>
	" don't like this, it won't close a window now;
nnoremap <leader>q :q<return>
"nnoremap <leader>e :e.<return>
    " ERROR! Vim has a bug where :e. opens a hidden buffer which cannot be traversed with :bprev
	" discussing here: https://vi.stackexchange.com/q/15011/16100;
	" solution is to use :Explore;
"nnoremap <leader>e :Explore<return>
	" trying to move away from this... :e .<tab> is better;
set wildcharm=<C-z> " wildchar for macros; 0 by default;
nnoremap <leader>e :e<space><C-z>

" faster navigation of quickfix list;
" should I be using a location list instead? investigate...
" no, location list is local to current window;
nnoremap <leader>g :copen<bar>vimgrep<space>[bar]<space>[/path/**]
	" g for grep
nnoremap <leader>n :cn<return>
nnoremap <leader>p :cp<return>

" modify search-replace and incremental search to use no-magic mode:
nnoremap / /\V
nnoremap <leader>r :%sno/x/y/g<left><left>

" by default, changelist-next will not move to last change in changelist
" if it currently at the last change (e.g., user previously hit g;);
" set changelist-next to first backtrack so that we get useful behavior
" if we are currently sitting at last change;
nnoremap g, g;g,g,

" fast edit vimrc (<cr> is same as <return>)
"nnoremap <leader>er :split ~/dotfiles/vimrc<cr>
"nnoremap <leader>sr :source ~/dotfiles/vimrc<cr>

" bar bar bar bar baz
" foo wfw w/w


" somehow turn this into a function that deletes and diffs swp files:
" :w ~/dotfiles/swp/%.diff
" :call delete(expand('~/dotfiles/swp/' . @% . '.swp'))
" :diffthis|vs /home/jglenn/dotfiles/swp/README.txt.diff | diffthis
" :execute "vs " . expand('~/dotfiles/swp/' . @% . '.diff')
" :execute 'diffthis|vs ' . expand('~/dotfiles/swp/' . @% . '.diff') . '|diffthis'

" What I really need is a macro that will delete any existing .swp files
" when I save a recovered file?

" Stopped 'Learn Vimscript the Hard Way' after 10, Training Your fingers.
"
